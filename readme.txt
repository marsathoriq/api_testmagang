pertama import database dengan nama user.sql
Pindahkan folder src menuju htdocs di xampp 
setelah itu masuk ke folder src beri command php -S localhost:8000 -t public
lalu test mengunakan postman dengan method post pilih body dan masukan syntax json ke bagian raw pilih json lalu set header dengan Content-Type : application/json

test case :
test 1
{
    "select": ["id","firstname","gender","city","status"],
    "limit": 4,
    "conditions": [
        {
            "type"  : "whereIn",
            "data"  : ["status",["Active","Pending"]]
        },
        {
            "type"  : "whereBetween",
            "data"  : ["id",[3,13]]
        },
        {
            "type"  : "whereNotBetween",
            "data"  : ["id",[5,8]]
        }
    ],
    "current_page": 1,
    "order": [
        {
            "field" : "firstname",
            "order" : "desc"
        },
        {
            "field" : "id",
            "order" : "desc"
        }
	]
}

test 2
{
    "select": ["id","firstname","gender","city","status"],
    "limit": 3,
    "conditions": [
        {
            "type"  : "whereColumn",
            "data"  : [
                ["firstname","=","Marsa"],
                ["lastname","<>","Ahmada"]
            ]
        },
        {
            "type"  : "whereNull",
            "data"  : ["gender"]
        },
        {
            "type"  : "orWhere",
            "data"  : ["id","=",2]
        }
    ],
    "current_page": 1,
    "order": [
        {
            "field" : "created_at",
            "order" : "desc"
        },
        {
            "field" : "id",
            "order" : "desc"
        }
	]
}
test 3
{
    "select": ["id","firstname","gender","city","status","created_at"],
    "limit": 4,
    "conditions": [
    ],
    "current_page": 2,
    "order": [
        {
            "field" : "created_at",
            "order" : "desc"
        },
        {
            "field" : "id",
            "order" : "desc"
        }
	]
}