<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function index(Request $request)
    {
        
        //getInput
        $select = $request->input('select');
        $limit = $request->input('limit');
        $cond = $request->input('conditions');
        $curr_page = $request->input('current_page');
        $order = $request->input('order');
        $data = DB::table('user');
        //where
        $len_cond = count($cond);
        for ($i = 0; $i < $len_cond; $i++) {
            
            switch ($cond[$i]["type"]) {
                case "whereColumn":
                    $len_data = count($cond[$i]["data"]);
                    for ($j = 0; $j < $len_data; $j++){
                        $data = $data->where($cond[$i]["data"][$j][0],$cond[$i]["data"][$j][1],$cond[$i]["data"][$j][2]);
                    }
                    break;
                case "whereNull":
                    $len_data = count($cond[$i]["data"]);
                    for ($j = 0; $j < $len_data; $j++){
                        $data = $data->whereNull($cond[$i]["data"][$j]);
                    }
                    break;
                case "whereNotIn":
                    $data = $data->whereNotIn($cond[$i]["data"][0],$cond[$i]["data"][1]);
                    break;
                case "whereIn":
                    $data = $data->whereIn($cond[$i]["data"][0],$cond[$i]["data"][1]);
                    break;
                case "whereNotBetween":
                    $data = $data->whereNotBetween($cond[$i]["data"][0],$cond[$i]["data"][1]);
                    break;
                case "whereBetween":
                    $data = $data->whereBetween($cond[$i]["data"][0],$cond[$i]["data"][1]);
                    break;
                case "orWhere":
                    if (isset($cond[$i]["data"])){
                        $data = $data->orWhere($cond[$i]["data"][0],$cond[$i]["data"][1],$cond[$i]["data"][2]);
                    }
                    break;
                default:
            }
        }
        //order
        $len_ord = count($order);
        if ($len_ord>=1){
            $ordStr = $order[0]['field']." ".$order[0]['order']; 
            for ($i = 1; $i < $len_ord; $i++) {
                $ordStr = $ordStr.",".$order[$i]['field']." ".$order[$i]['order'];
            }
        }
        $data = $data->orderByRaw($ordStr);
        //get selected data
        $data = $data->get($select);
        $totalData = count($data);
        $totalPages = intdiv($totalData,  $limit) + 1;
        if ($totalData % $limit == 0){
            $totalPages = $totalPages - 1;
        }
        $dataShow = $data->skip(($curr_page-1)*$limit)->take($limit);
        $res = [
            'limit' => $limit,
            'total_row'=>min($limit,$totalData), 
            'total_data'=>$totalData, 
            'total_page'=>$totalPages, 
            'current_page'=>$curr_page,
            'data' => $dataShow 
        ];
        return response () -> json($res);
        //return view('user.index', ['users' => $users]);
    }

    //
}
