-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Bulan Mei 2020 pada 08.39
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `user`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(256) NOT NULL,
  `lastname` varchar(256) NOT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `email` varchar(256) NOT NULL,
  `city` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `gender`, `status`, `email`, `city`, `address`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'Marsa', 'Ahmada', 'male', 'Pending', 'marsaahmada@gmail.com', 'Bandung', 'Jl Tubagus Ismail VIII', '0895357870007', '2020-05-06', '2020-05-07'),
(2, 'Eko', 'Sugito', 'male', 'Active', 'ek@gmail.com', 'Banyuwangi', 'Parirejo', '0899999', '2020-05-03', '2020-05-04'),
(3, 'Zaenal', 'Abidin', 'male', 'Banned', 'zen@mail.com', 'Jember', 'Jl jalan', '080000', '2020-05-01', '2020-05-07'),
(4, 'Eki', 'Sulistya', 'female', 'Active', 'eki@gmail.com', 'Lumajang', 'Rowokangkung', '0888888', '2020-05-02', '2020-05-04'),
(5, 'Nana', 'Khairul', 'female', 'Pending', 'nana@m.com', 'Malang', 'Kota', '0800001', '2020-05-01', '2020-05-01'),
(6, 'Thoriq', 'Ahmada', 'male', 'Active', 'thor@gmail.com', 'Banyuwangi', 'Parijatah', '0895357870009', '2020-05-02', '2020-05-05'),
(7, 'Marsa', 'Thoriq', 'male', 'Banned', 'rick@gmail.com', 'Surabaya', 'Gubeng', '0800002', '2020-05-03', '2020-05-07'),
(8, 'Marsat', 'Thoriq', 'male', 'Active', 'rickt@gmail.com', 'Surabaya', 'Gubeng', '08000022', '2020-05-04', '2020-05-07'),
(9, 'Nina', 'Khairul', 'female', 'Active', 'nana@mail.com', 'Malang', 'Kota', '08000016', '2020-05-01', '2020-05-03'),
(10, 'Zaen', 'Abid', 'male', 'Pending', 'zaen@mail.com', 'Jember', 'Jl jalan 2', '08000', '2020-05-02', '2020-05-07'),
(11, 'Marsa', 'Lumina', 'female', 'Active', 'marsaaa@gmail.com', 'Bandung', 'Jl Tubagus Ismail VIII', '0895357870008', '2020-05-04', '2020-05-07'),
(12, 'Marsa', 'Ekota', NULL, 'Banned', 'mam@.com', 'Surabaya', 'Jl', '08953578700071', '2020-05-04', '2020-05-07'),
(13, 'Marsa', 'Eko', NULL, 'Active', 'mm@.com', 'Malang', 'Jl', '08953578700071', '2020-05-04', '2020-05-07');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
